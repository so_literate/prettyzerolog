# Pretty zerolog

Prettyzerolog is a pretty logging with focus on readability, 
based on the `zerolog.ConsoleWriter` but with some added features 
like json pretty printing and log line separator.

It is fork from https://github.com/UnnoTed/horizontal with some fixes.

![prettyzerolog](https://i.imgur.com/E6CHIqF.png)

`go get gitlab.com/so_literate/prettyzerolog`

```go
package main

import (
	"os"

	"github.com/rs/zerolog/log"
	"gitlab.com/so_literate/prettyzerolog"
)

type fields struct {
	Field1 string
	Field2 int
	Field3 interface{}
}

func main() {
	logger := log.Output(prettyzerolog.ConsoleWriter{Out: os.Stdout})
	logger.Debug().Msg("hello")

	f := &fields{
		Field1: "field 1",
		Field2: 2,
		Field3: true,
	}

	logger.Info().Interface("fields", f).Str("text", "some text").Msg("some fields")
}

```
