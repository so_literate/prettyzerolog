module gitlab.com/so_literate/prettyzerolog

go 1.13

require (
	github.com/fatih/color v1.7.0
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/nwidger/jsoncolor v0.1.0
	github.com/olekukonko/ts v0.0.0-20171002115256-78ecb04241c0
	github.com/rs/zerolog v1.15.0
)
